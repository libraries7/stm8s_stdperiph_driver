/*
 * \file stm8s_systick.h
 * \brief   systick timer simulation implementation
 *
 * Copyright (C) 2015 Houtouridis Christos <houtouridis.ch@gmail.com>
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Houtouridis Christos. The intellectual
 * and technical concepts contained herein are proprietary to
 * Houtouridis Christos and are protected by copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Houtouridis Christos.
 *
 * Author:     Houtouridis Christos <houtouridis.ch@gmail.com>
 * Date:       11/2015
 * Version:    0.2
 */

#ifndef __stm8s_systick_h__
#define __stm8s_systick_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <stm8s.h>
#include <stm8s_itc.h>
#include <stm8s_clk.h>
#include <stm8s_tim4.h>

/*
 * Also defined in types.h
 */
#ifndef  _CLOCK_T_
#define  _CLOCK_T_   unsigned long     /* clock() */
typedef _CLOCK_T_ clock_t;             /*!< CPU time type */
#endif
#ifndef _TIME_T_
#define  _TIME_T_ long                 /* time() */
typedef _TIME_T_ time_t;               /*!< date/time in unix secs past 1-Jan-70 type for 68 years*/
#endif

/* ======== Core Functionalities ============ */

void SysTick_Callback (void);

void SysTick_DeInit (void);
clock_t SysTick_Init (clock_t sf);



#ifdef __cplusplus
}
#endif

#endif   //#ifndef __stm8s_systick_h__

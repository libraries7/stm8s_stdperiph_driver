/*
 * \file stm8s_chrono.h
 * \brief   Chronograph implementation
 *
 * Copyright (C) 2016 Houtouridis Christos <houtouridis.ch@gmail.com>
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Houtouridis Christos. The intellectual
 * and technical concepts contained herein are proprietary to
 * Houtouridis Christos and are protected by copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Houtouridis Christos.
 *
 */

#ifndef __stm8s_chrono_h__
#define __stm8s_chrono_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <stm8s.h>
#include <stm8s_itc.h>
#include <stm8s_clk.h>
#include <stm8s_tim4.h>
#include <stm8s_tim2.h>

/*
 *  ================= User defines ===============
 */
#define CHR_PERIODIC_EN         (1)
#define CHR_PERIODIC_ENTRIES    (10)

/*
 *  ================= Chronograph data types ===============
 */
typedef uint32_t clock_t;              //!< The counter data type
typedef enum {
   CHR_STOP = 0,
   CHR_RUN
} chr_state_en;

typedef enum {
   CHR_PER_STOP = 0,
   CHR_PER_RUN
} chr_periodic_en;

/*!<
 * Chronograph data type
 */
typedef struct {
   clock_t        ticks;   //!< Chronograph time
   uint32_t       freq;    //!< Chronograph Interrupt Frequency
   chr_state_en   state;   //!< Chronographs run flag
   #if CHR_PERIODIC_EN == 1
   chr_periodic_en   periodic_state;   //!< Periodic run flag
   #endif
}chrono_t;

#if CHR_PERIODIC_EN == 1
typedef void (*periodicfun_t) (void);     //!< Pointer to void function (void) to use with periodic

/*!
 * periodic Table data type
 */
typedef struct
{
   periodicfun_t  fun;
   clock_t        tic;
}chr_periodic_t;
#endif

/*
 * ========== Public API =============
 */
void chr_deinit (void);
clock_t chr_init (clock_t cf);

clock_t get_freq (void);
clock_t set_freq (clock_t f);

#if CHR_PERIODIC_EN == 1
void chr_periodic_add (periodicfun_t pfun, clock_t tic);
void chr_periodic_rem (periodicfun_t pfun);
void chr_periodic_ctl (chr_periodic_en state);
#endif


void chr_start (clock_t v);
void chr_continue (void);
void chr_stop (void);
void chr_reset (clock_t v);
clock_t chr_clock (void);

#ifdef __cplusplus
}
#endif

#endif   // #ifndef __stm8s_chrono_h__

/*
 * \file stm8s_chrono.h
 * \brief   Chronograph implementation
 *
 * Copyright (C) 2016 Houtouridis Christos <houtouridis.ch@gmail.com>
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Houtouridis Christos. The intellectual
 * and technical concepts contained herein are proprietary to
 * Houtouridis Christos and are protected by copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Houtouridis Christos.
 *
 */

#ifndef __stm8s_chrono_tim2_h__
#define __stm8s_chrono_tim2_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <stm8s.h>
#include <limits.h>
#include <stm8s_itc.h>
#include <stm8s_clk.h>
#include <stm8s_tim2.h>

/*
 *  ================= User defines ===============
 */
#define CHR_PERIODIC_EN          (1)
#define CHR_PERIODIC_ENTRIES     (10)
#define CHR_MAX_AUTORELOAD       (1000)
#define CHR_SHADOW_REGISTERS     (0)

/*
 * Also defined in types.h
 */
#ifndef  _CLOCK_T_
#define  _CLOCK_T_   unsigned long     /* clock() */
typedef _CLOCK_T_ clock_t;             /*!< CPU time type */
#endif

/*!
 * To read signed CPU time use \ref sclock(). Using sclock() insteed of clock()
 * can help the user program to find out when the cpu clock variable rolls over max value.
 *
 * for ex:
 *
 *    // wait 10 __sticks;
 *    sclock_t mark = sclock(), diff, n;
 *    do {
 *       n = sclock();
 *       diff = ((n - mark) >=0 ) ?
 *           n-mark :   // if not rolled
 *          -n-mark ;   // if rolled
 *    } while (diff < 10);
 *
 * \note
 *    DO NOT exceed \ref _SCLOCK_T_MAX_VALUE_ value for this kind of calculations
 *
 * \sa _SCLOCK_DIFF Macro.
 */
typedef int32_t   sclock_t;

/*
 * =========== Helper macros ===============
 */
#define _CLOCK_T_MAX_VALUE_            (ULONG_MAX) //!< Helper macro for maximum signed CPU time calculations
#define _SCLOCK_T_MAX_VALUE_           (LONG_MAX)  //!< Helper macro for maximum signed CPU time calculations

//! Absolute value macro
#define _SC_ABS(_x)                    ( ((_x) < 0) ? -(_x) : (_x) )

/*!
 *  Calculate the positive time difference of _t2 and _t1, where
 *  _t1, _t2 are sclock_t values
 *  \note
 *    _t2 is AFTER _t1
 */
#define _SCLOCK_DIFF(_t2, _t1)         ( (((_t2)-(_t1)) >= 0) ? ((_t2)-(_t1)) : _SC_ABS(-(_t2)-(_t1)) )


/*!
 *  Calculate the positive time difference of _t2_ and _t1_, where
 *  _t1_, _t2_ are clock_t values
 *  \note
 *    _t2_ is AFTER _t1_
 *
 *  ex:
 *  0   1   2   3   4   5   6   7   8   9
 *      ^                       ^
 *      |                       |
 *      a                       b
 *
 * if : t1=a, t2=b      then  dt = b-a             = t2 - t1
 * if : t1=b, t2=a      then  dt = 9 - (b-a) + 1   = UMAX - (t1-t2) + 1
 *
 */
#define _CLOCK_DIFF(_t2_, _t1_)     ( ((_t2_)>(_t1_)) ? ((_t2_)-(_t1_)) : (_CLOCK_T_MAX_VALUE_ - ((_t1_) - (_t2_)) + 1) )

/*
 *  ================= Chronograph data types ===============
 */

typedef enum {
   CHR_STOP = 0,
   CHR_RUN
} chr_state_en;

typedef enum {
   CHR_PER_STOP = 0,
   CHR_PER_RUN
} chr_periodic_en;

/*!<
 * Chronograph data type
 */
typedef struct {
   chr_state_en   state;   //!< Chronographs run flag
   clock_t        ticks;   //!< Chronograph time
   #if CHR_SHADOW_REGISTERS == 1
   clock_t        ticks_a, ticks_b;    //!< Chronograph shadow registers
   #endif
   uint32_t       freq;    //!< Chronograph Interrupt Frequency
   #if CHR_PERIODIC_EN == 1
   chr_periodic_en   per_state;  //!< Periodic run flag
   clock_t           per_ticks;  //!< Periodic clock
   clock_t           per_max;    //!< Periodic max count
   #endif
}chrono_t;

#if CHR_PERIODIC_EN == 1
typedef void (*periodicfun_t) (void);     //!< Pointer to void function (void) to use with periodic

/*!
 * periodic Table data type
 */
typedef struct
{
   periodicfun_t  fun;
   clock_t        tic;
}chr_periodic_t;
#endif

/*
 * ========== Public API =============
 */
void chr_deinit (void);
uint32_t chr_init (uint32_t cf);

uint32_t get_freq (void);
uint32_t set_freq (uint32_t f);

#if CHR_PERIODIC_EN == 1
void chr_per_max (clock_t max);
void chr_per_add (periodicfun_t pfun, clock_t tic);
void chr_per_rem (periodicfun_t pfun);
void chr_per_start (clock_t v);
void chr_per_continue (void);
void chr_per_stop (void);
void chr_per_reset (clock_t v);
clock_t chr_per_clock (void);
#endif


void chr_start (clock_t v);
void chr_continue (void);
void chr_stop (void);
void chr_reset (clock_t v);
clock_t chr_clock (void);

#ifdef __cplusplus
}
#endif

#endif   // #ifndef __stm8s_chrono_h__

/*
 * \file stm8s_chrono.c
 * \brief   Chronograph implementation
 *
 * Copyright (C) 2016 Houtouridis Christos <houtouridis.ch@gmail.com>
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Houtouridis Christos. The intellectual
 * and technical concepts contained herein are proprietary to
 * Houtouridis Christos and are protected by copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Houtouridis Christos.
 *
 */
#include <stm8s_chrono.h>

/* ================== Static Time Data ======================= */

static chrono_t _chr;

#if CHR_PERIODIC_EN == 1
static chr_periodic_t  _periodic[CHR_PERIODIC_ENTRIES];
#endif

/*!<
 *  periodic Table holds all requested entries for periodic function calls.
 *  \note
 *    All the entries will run in privileged mode and will use the main
 *    stack.
 */

/*!
 * \brief
 *    Chronograph timer configuration
 */
static clock_t _tim_config (clock_t cf)
{
   uint32_t clk, tclk;  // System clock, timer clock
   uint8_t t, d;        // timer auto-reload value and divider
   TIM4_Prescaler_TypeDef pr;

   clk = CLK_GetClockFreq ();
   tclk=clk;
   for (tclk=0,pr=TIM4_PRESCALER_1 ; pr<=TIM4_PRESCALER_128; ++pr) {
      d = 0x01<<pr;
      tclk = clk/d;
      if ((tclk/U8_MAX) <= cf)
         break;
   }
   if (pr>TIM4_PRESCALER_128)
      return (clock_t)0;   // Give up
   else {
      t = tclk / cf;    // calculate integer timer value and update system freq variable
      cf = tclk / t;

      // Setup TIM4
      CLK_PeripheralClockConfig (CLK_PERIPHERAL_TIMER4, ENABLE);
      TIM4_PrescalerConfig ((TIM4_Prescaler_TypeDef) pr, TIM4_PSCRELOADMODE_UPDATE);
      TIM4_SetAutoreload (t);
      TIM4_ITConfig (TIM4_IT_UPDATE, ENABLE);
      TIM4_Cmd (ENABLE);

      return cf;  // Return actual system frequency
   }
}

/*!
 * \brief SysTick Handler
 */
INTERRUPT_HANDLER (Chrono_Handler, ITC_IRQ_TIM4_OVF)
{
   #if CHR_PERIODIC_EN == 1
   int i;
   #endif

   //Update Exported Counters
   if (_chr.state == CHR_RUN)
      ++_chr.ticks;

   #if CHR_PERIODIC_EN == 1
   for (i=0 ; _chr.periodic_state && i<CHR_PERIODIC_ENTRIES ; ++i) {
      if (_periodic[i].fun && !(_chr.ticks %_periodic[i].tic))
         _periodic[i].fun ();
   }
   #endif
   TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
}


/*
 * ================ Public API ===================
 */

/*!
 * De-Initialize the Chronograph
 */
void chr_deinit (void) {
   TIM4_DeInit ();
   CLK_PeripheralClockConfig (CLK_PERIPHERAL_TIMER4, DISABLE);
}


/*!
 * Initialize and Start the Chronograph's timer with freq
 */
clock_t chr_init (clock_t cf) {
   _chr.freq = _tim_config (cf);
   //if ( !__get_interrupt_state () )
      enableInterrupts ();
   return _chr.freq;
}


/*!
 * \brief Get the chronograph's freq.
 */
inline clock_t get_freq (void){
   return _chr.freq;
}

/*!
 * \brief Reconfigure the chronograph and update freq
 */
inline clock_t set_freq (clock_t sf) {
   return _chr.freq = _tim_config (sf);
}

#if CHR_PERIODIC_EN == 1
/*!
 * \brief
 *    Add a function to periodic
 *
 * \param   pfun  Pointer to function
 * \param   tic   Tick interval. periodic will run the \a pfun every \b tic Ticks
 *
 * \note
 *    All the entries will run in privileged mode and will use the main
 *    stack.
 */
void chr_periodic_add (periodicfun_t pfun, clock_t tic)
{
   int i;
   for (i=0 ; i<CHR_PERIODIC_ENTRIES ; ++i)
      if (!_periodic[i].fun)
      {
         _periodic[i].fun = pfun;
         _periodic[i].tic = tic;
         return;
      }
}

/*!
 * \brief
 *    Remove a function from periodic
 *
 * \param   pfun Pointer to function
 */
void chr_periodic_rem (periodicfun_t pfun)
{
   int i;
   for (i=0 ; i<CHR_PERIODIC_ENTRIES ; ++i)
      if (_periodic[i].fun == pfun)
         _periodic[i].fun = (void*)0;
}

/*!
 * \brief
 *    Statr or stop periodic functionality
 *
 * \param   pfun Pointer to function
 */
inline void chr_periodic_ctl (chr_periodic_en state) {
   _chr.periodic_state = state;
}
#endif

/*!
 * \brief
 *    Start or continue Chronograph from a selected value
 *
 * \param   v Starting value
 *    \arg  (clock_t)-1    continue from the current counter value
 *    \arg  != (clock_t)-1 Start from this value
 */
inline void chr_start (clock_t v)
{
   if (v != (clock_t)(-1))
      _chr.ticks = v;
   _chr.state = CHR_RUN;
}

/*!
 * \brief
 *    Continue Chronograph from current value
 */
inline void chr_continue (void) {
   _chr.state = CHR_RUN;
}

/*!
 * \brief
 *    Stop Chronograph
 */
inline void chr_stop (void) {
   _chr.state = CHR_STOP;
}

/*!
 * \brief
 *    Reset Chronograph's value
 */
inline void chr_reset (clock_t v) {
   _chr.ticks = v;
}

/*!
 * \brief
 *    Get Chronograph's value
 */
inline clock_t chr_clock (void) {
   return _chr.ticks;
}


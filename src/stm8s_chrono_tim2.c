/*
 * \file stm8s_chrono_tim2.c
 * \brief
 *    Chronograph implementation based on TIM2
 *
 * Copyright (C) 2016 Houtouridis Christos <houtouridis.ch@gmail.com>
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Houtouridis Christos. The intellectual
 * and technical concepts contained herein are proprietary to
 * Houtouridis Christos and are protected by copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Houtouridis Christos.
 *
 */
#include <stm8s_chrono_tim2.h>

/*
 * ========== Static Data and functions ============
 */
static chrono_t _chr;
#if CHR_PERIODIC_EN == 1
static chr_periodic_t  _periodic[CHR_PERIODIC_ENTRIES];
/*!<
 *  periodic Table holds all requested entries for periodic function calls.
 *  \note
 *    All the entries will run in privileged mode and will use the main
 *    stack.
 */
#endif

/*!
 * \brief
 *    Chronograph timer configuration
 */
static uint32_t _tim_config (uint32_t cf)
{
   uint32_t clk, tclk;  // System clock, timer clock
   uint16_t t, d;       // timer auto-reload value and divider
   TIM2_Prescaler_TypeDef pr;

   clk = CLK_GetClockFreq ();
   tclk=clk;
   for (tclk=0,pr=TIM2_PRESCALER_1 ; pr<=TIM2_PRESCALER_32768; ++pr) {
      d = 0x01<<pr;
      tclk = clk/d;
      if ((tclk/CHR_MAX_AUTORELOAD) <= cf)
         break;
   }
   if (pr>TIM2_PRESCALER_32768)
      return (uint32_t)0;   // Give up
   else {
      t = tclk / cf;    // calculate integer timer value and update system freq variable
      cf = tclk / t;

      // Setup TIM2
      CLK_PeripheralClockConfig (CLK_PERIPHERAL_TIMER2, ENABLE);
      TIM2_PrescalerConfig ((TIM2_Prescaler_TypeDef) pr, TIM2_PSCRELOADMODE_UPDATE);
      TIM2_SetAutoreload (t);
      TIM2_ITConfig (TIM2_IT_UPDATE, ENABLE);
      TIM2_Cmd (ENABLE);

      return cf;  // Return actual system frequency
   }
}

#if CHR_SHADOW_REGISTERS == 1
/*
 * \brief
 *    Tick error repair routine
 */
void _cure_ticks (void)
{
   if       (_chr.ticks_a == _chr.ticks_b)  _chr.ticks = _chr.ticks_a;  // Repair ticks
   else if  (_chr.ticks == _chr.ticks_b)    _chr.ticks_a = _chr.ticks;  // Repair ticks_a
   else if  (_chr.ticks == _chr.ticks_b)    _chr.ticks_a = _chr.ticks;  // Repair ticks_b
   else {
      // The error is non-reversible, we will try our best
      _chr.ticks = _chr.ticks/3 + _chr.ticks_a/3 + _chr.ticks_b/3;
      _chr.ticks_a = _chr.ticks_b = _chr.ticks;
   }
}
#endif

/*
 * ========== IRQ ============
 */

/*!
 * \brief SysTick Handler
 */
INTERRUPT_HANDLER (Chrono_Handler, ITC_IRQ_TIM2_OVF)
{
   #if CHR_PERIODIC_EN == 1
   int i;
   #endif

   //Update Exported Counters
   if (_chr.state == CHR_RUN) {
      #if CHR_SHADOW_REGISTERS == 1
      if (!(_chr.ticks == _chr.ticks_a && _chr.ticks == _chr.ticks_b))
         _cure_ticks ();
      ++_chr.ticks_a; ++_chr.ticks_b;
      #endif
      ++_chr.ticks;
   }
   #if CHR_PERIODIC_EN == 1
   if (_chr.per_state == CHR_PER_RUN) {
      if (++_chr.per_ticks >= _chr.per_max)
         _chr.per_ticks = 0;
      for (i=0 ; i<CHR_PERIODIC_ENTRIES ; ++i) {
         if (_periodic[i].fun && !(_chr.per_ticks %_periodic[i].tic))
            _periodic[i].fun ();
      }
   }
   #endif
   TIM2_ClearITPendingBit(TIM2_IT_UPDATE);
}


/*
 * ================ Public API ===================
 */

/*!
 * De-Initialize the Chronograph
 */
void chr_deinit (void) {
   TIM2_DeInit ();
   CLK_PeripheralClockConfig (CLK_PERIPHERAL_TIMER2, DISABLE);
}


/*!
 * Initialize and Start the Chronograph's timer with freq
 */
uint32_t chr_init (uint32_t cf) {
   _chr.freq = _tim_config (cf);
   //if ( !__get_interrupt_state () )
      enableInterrupts ();
   return _chr.freq;
}


/*!
 * \brief Get the chronograph's freq.
 */
inline uint32_t get_freq (void){
   return _chr.freq;
}

/*!
 * \brief Reconfigure the chronograph and update freq
 */
inline uint32_t set_freq (uint32_t sf) {
   return _chr.freq = _tim_config (sf);
}

/*!
 * \brief
 *    Start or continue Chronograph from a selected value
 *
 * \param   v Starting value
 *    \arg  (clock_t)-1    continue from the current counter value
 *    \arg  != (clock_t)-1 Start from this value
 */
inline void chr_start (clock_t v)
{
   if (v != (clock_t)(-1)) {
      _chr.ticks = v;
      #if CHR_SHADOW_REGISTERS == 1
      _chr.ticks_a = _chr.ticks_b = v;
      #endif
   }
   _chr.state = CHR_RUN;
}

/*!
 * \brief
 *    Continue Chronograph from current value
 */
inline void chr_continue (void) {
   _chr.state = CHR_RUN;
}

/*!
 * \brief
 *    Stop Chronograph
 */
inline void chr_stop (void) {
   _chr.state = CHR_STOP;
}

/*!
 * \brief
 *    Reset Chronograph's value
 */
inline void chr_reset (clock_t v) {
   _chr.ticks = v;
   #if CHR_SHADOW_REGISTERS == 1
   _chr.ticks_a = _chr.ticks_b = v;
   #endif
}

/*!
 * \brief
 *    Get Chronograph's value
 */
inline clock_t chr_clock (void) {
   return _chr.ticks;
}


#if CHR_PERIODIC_EN == 1

/*!
 * \brief
 *    Set periodic tick max count value
 * \note
 *    A selection of maximum common multiple of all periodic events
 *    is a good selection.
 */
inline void chr_per_max (clock_t max) {
   _chr.per_max = max;
}

/*!
 * \brief
 *    Add a function to periodic
 *
 * \param   pfun  Pointer to function
 * \param   tic   Tick interval. periodic will run the \a pfun every \b tic Ticks
 *
 * \note
 *    All the entries will run in privileged mode and will use the main
 *    stack.
 */
void chr_per_add (periodicfun_t pfun, clock_t tic)
{
   int i;
   for (i=0 ; i<CHR_PERIODIC_ENTRIES ; ++i)
      if (!_periodic[i].fun)
      {
         _periodic[i].fun = pfun;
         _periodic[i].tic = tic;
         return;
      }
}

/*!
 * \brief
 *    Remove a function from periodic
 *
 * \param   pfun Pointer to function
 */
void chr_per_rem (periodicfun_t pfun)
{
   int i;
   for (i=0 ; i<CHR_PERIODIC_ENTRIES ; ++i)
      if (_periodic[i].fun == pfun)
         _periodic[i].fun = (void*)0;
}

/*!
 * \brief
 *    Start or continue Periodic from a selected value
 *
 * \param   v Starting value
 *    \arg  (clock_t)-1    continue from the current counter value
 *    \arg  != (clock_t)-1 Start from this value
 */
inline void chr_per_start (clock_t v) {
   if (v != (clock_t)(-1))
      _chr.per_ticks = v;
   _chr.per_state = CHR_PER_RUN;
}

/*!
 * \brief
 *    Continue Periodic from current value
 */
inline void chr_per_continue (void) {
   _chr.per_state = CHR_PER_RUN;
}

/*!
 * \brief
 *    Stop Periodic
 */
inline void chr_per_stop (void) {
   _chr.per_state = CHR_PER_STOP;
}

/*!
 * \brief
 *    Reset Periodic's value
 */
inline void chr_per_reset (clock_t v) {
   _chr.per_ticks = v;
}

/*!
 * \brief
 *    Get Periodic's value
 */
inline clock_t chr_per_clock (void) {
   return _chr.per_ticks;
}
#endif


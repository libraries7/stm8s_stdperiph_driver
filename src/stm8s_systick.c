/*
 * \file stm8s_systick.c
 * \brief   systick timer simulation implementation
 *
 * Copyright (C) 2015 Houtouridis Christos <houtouridis.ch@gmail.com>
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Houtouridis Christos. The intellectual
 * and technical concepts contained herein are proprietary to
 * Houtouridis Christos and are protected by copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Houtouridis Christos.
 *
 * Author:     Houtouridis Christos <houtouridis.ch@gmail.com>
 * Date:       11/2015
 * Version:    0.2
 */

#include <stm8s_systick.h>

/* ================== Static Time Data ======================= */

static clock_t __sys_freq;             /*!< SysTick Interrupt Frequency */

/*!
 * \brief
 *    Systick timer configuration
 */
static clock_t _tim_config (clock_t sf)
{
   uint32_t clk, tclk;  // System clock, timer clock
   uint8_t t, d;        // timer auto-reload value and divider
   TIM4_Prescaler_TypeDef pr;

   clk = CLK_GetClockFreq ();
   tclk=clk;
   for (tclk=0,pr=TIM4_PRESCALER_1 ; pr<=TIM4_PRESCALER_128; ++pr) {
      d = 0x01<<pr;
      tclk = clk/d;
      if ((tclk/U8_MAX) <= sf)
         break;
   }
   if (pr>TIM4_PRESCALER_128)
      return (clock_t)0;   // Give up
   else {
      t = tclk / sf;    // calculate integer timer value and update system freq variable
      sf = tclk / t;

      // Setup TIM4
      CLK_PeripheralClockConfig (CLK_PERIPHERAL_TIMER4, ENABLE);
      TIM4_PrescalerConfig ((TIM4_Prescaler_TypeDef) pr, TIM4_PSCRELOADMODE_UPDATE);
      TIM4_SetAutoreload (t);
      TIM4_ITConfig (TIM4_IT_UPDATE, ENABLE);
      TIM4_Cmd (ENABLE);

      return sf;  // Return actual system frequency
   }
}

__weak void SysTick_Callback (void) {
   /*! \note
    *    This function Should not be modified, when the callback is needed,
    *    the SysTick_Callback could be implemented in the user file
    */
}

/*!
 * \brief SysTick Handler
 */
INTERRUPT_HANDLER (SysTick_Handler, ITC_IRQ_TIM4_OVF)
{
   SysTick_Callback ();
   TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
}

/*!
 * \brief
 *    This is the SysTick ISR
 * \note
 *    To reduce the overhead of yet another API function to call from user's
 *    driver implementation. HAL defines a weak ISR here and a weak Callback.
 */
void SysTick_Handler(void) {
   SysTick_Callback ();
}


/*!
 * De-Initialize the SysTick
 */
void SysTick_DeInit (void) {
   TIM4_DeInit ();
   CLK_PeripheralClockConfig (CLK_PERIPHERAL_TIMER4, DISABLE);
}


/*!
 * Initialize and Start the SysTick with sys_freq
 */
clock_t SysTick_Init (clock_t sf) {
   __sys_freq = _tim_config (sf);
   //if ( !__get_interrupt_state () )
      enableInterrupts ();
   return __sys_freq;
}

